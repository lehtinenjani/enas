﻿/*
 * Created by nxtSTUDIO.
 * User: dmidro
 * Date: 9/23/2018
 * Time: 2:03 PM
 * 
 */

using System;
using System.Drawing;
using NxtControl.GuiFramework;

namespace HMI.Main.Symbols.ProductOrder
{
	/// <summary>
	/// Description of sDefault.
	/// </summary>
	public partial class sDefault : NxtControl.GuiFramework.HMISymbol
	{
		public sDefault()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
		}
		
		void DrawnButton2Click(object sender, EventArgs e)
		{
		  int red = twoStateButton1.Checked ? 1 : 0;
		  int green = twoStateButton2.Checked ? 1 : 0;
		  this.FireEvent_CNF(false, (short)green, (short)red);
		}
		
		void DrawnButton1Click(object sender, EventArgs e)
		{
			this.FireEvent_CNF(true, 0, 0);
		}
	}
}
