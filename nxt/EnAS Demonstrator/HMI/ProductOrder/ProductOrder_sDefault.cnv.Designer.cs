﻿/*
 * Created by nxtSTUDIO.
 * User: dmidro
 * Date: 9/23/2018
 * Time: 2:03 PM
 * 
 */
using System;
using System.ComponentModel;
using System.Collections;
using NxtControl.GuiFramework;

namespace HMI.Main.Symbols.ProductOrder
{
	/// <summary>
	/// Summary description for sDefault.
	/// </summary>
	partial class sDefault
	{

		#region Component Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.drawnButton1 = new NxtControl.GuiFramework.DrawnButton();
			this.drawnButton2 = new NxtControl.GuiFramework.DrawnButton();
			this.twoStateButton1 = new NxtControl.GuiFramework.TwoStateButton();
			this.twoStateButton2 = new NxtControl.GuiFramework.TwoStateButton();
			// 
			// drawnButton1
			// 
			this.drawnButton1.Bounds = new NxtControl.Drawing.RectF(((float)(140D)), ((float)(63D)), ((float)(100D)), ((float)(25D)));
			this.drawnButton1.Brush = new NxtControl.Drawing.Brush("ButtonBrush");
			this.drawnButton1.Font = new NxtControl.Drawing.Font("ButtonFont");
			this.drawnButton1.InnerBorderColor = new NxtControl.Drawing.Color("ButtonInnerBorderColor");
			this.drawnButton1.Name = "drawnButton1";
			this.drawnButton1.Pen = new NxtControl.Drawing.Pen("ButtonPen");
			this.drawnButton1.Radius = 4D;
			this.drawnButton1.Text = "Reset";
			this.drawnButton1.TextColor = new NxtControl.Drawing.Color("ButtonTextColor");
			this.drawnButton1.TextColorMouseDown = new NxtControl.Drawing.Color("ButtonTextColorMouseDown");
			this.drawnButton1.Use3DEffect = false;
			this.drawnButton1.Click += new System.EventHandler(this.DrawnButton1Click);
			// 
			// drawnButton2
			// 
			this.drawnButton2.Bounds = new NxtControl.Drawing.RectF(((float)(140D)), ((float)(16D)), ((float)(100D)), ((float)(25D)));
			this.drawnButton2.Brush = new NxtControl.Drawing.Brush("ButtonBrush");
			this.drawnButton2.Font = new NxtControl.Drawing.Font("ButtonFont");
			this.drawnButton2.InnerBorderColor = new NxtControl.Drawing.Color("ButtonInnerBorderColor");
			this.drawnButton2.Name = "drawnButton2";
			this.drawnButton2.Pen = new NxtControl.Drawing.Pen("ButtonPen");
			this.drawnButton2.Radius = 4D;
			this.drawnButton2.Text = "Order";
			this.drawnButton2.TextColor = new NxtControl.Drawing.Color("ButtonTextColor");
			this.drawnButton2.TextColorMouseDown = new NxtControl.Drawing.Color("ButtonTextColorMouseDown");
			this.drawnButton2.Use3DEffect = false;
			this.drawnButton2.Click += new System.EventHandler(this.DrawnButton2Click);
			// 
			// twoStateButton1
			// 
			this.twoStateButton1.Bounds = new NxtControl.Drawing.RectF(((float)(8D)), ((float)(17D)), ((float)(55D)), ((float)(24D)));
			this.twoStateButton1.DrawStyle = NxtControl.GuiFramework.TwoStateButton.ButtonDrawStyle.CheckBox;
			this.twoStateButton1.FalseBrush = new NxtControl.Drawing.Brush("ButtonFalseBrush");
			this.twoStateButton1.FalseText = "Red";
			this.twoStateButton1.Font = new NxtControl.Drawing.Font("ButtonFont");
			this.twoStateButton1.InnerBorderColor = new NxtControl.Drawing.Color("ButtonInnerBorderColor");
			this.twoStateButton1.Name = "twoStateButton1";
			this.twoStateButton1.Pen = new NxtControl.Drawing.Pen("ButtonPen");
			this.twoStateButton1.Radius = 8D;
			this.twoStateButton1.TextColorFalse = new NxtControl.Drawing.Color("ButtonTextColorFalse");
			this.twoStateButton1.TextColorTrue = new NxtControl.Drawing.Color("ButtonTextColorTrue");
			this.twoStateButton1.TrueText = "Red";
			this.twoStateButton1.Use3DEffect = false;
			// 
			// twoStateButton2
			// 
			this.twoStateButton2.Bounds = new NxtControl.Drawing.RectF(((float)(73D)), ((float)(17D)), ((float)(55D)), ((float)(24D)));
			this.twoStateButton2.DrawStyle = NxtControl.GuiFramework.TwoStateButton.ButtonDrawStyle.CheckBox;
			this.twoStateButton2.FalseBrush = new NxtControl.Drawing.Brush("ButtonFalseBrush");
			this.twoStateButton2.FalseText = "Green";
			this.twoStateButton2.Font = new NxtControl.Drawing.Font("ButtonFont");
			this.twoStateButton2.InnerBorderColor = new NxtControl.Drawing.Color("ButtonInnerBorderColor");
			this.twoStateButton2.Name = "twoStateButton2";
			this.twoStateButton2.Pen = new NxtControl.Drawing.Pen("ButtonPen");
			this.twoStateButton2.Radius = 8D;
			this.twoStateButton2.TextColorFalse = new NxtControl.Drawing.Color("ButtonTextColorFalse");
			this.twoStateButton2.TextColorTrue = new NxtControl.Drawing.Color("ButtonTextColorTrue");
			this.twoStateButton2.TrueText = "Green";
			this.twoStateButton2.Use3DEffect = false;
			// 
			// sDefault
			// 
			this.Name = "sDefault";
			this.Shapes.AddRange(new System.ComponentModel.IComponent[] {
									this.drawnButton1,
									this.drawnButton2,
									this.twoStateButton1,
									this.twoStateButton2});
			this.SymbolSize = new System.Drawing.Size(600, 400);
		}
		private NxtControl.GuiFramework.TwoStateButton twoStateButton2;
		private NxtControl.GuiFramework.TwoStateButton twoStateButton1;
		private NxtControl.GuiFramework.DrawnButton drawnButton2;
		private NxtControl.GuiFramework.DrawnButton drawnButton1;
		#endregion
	}
}
