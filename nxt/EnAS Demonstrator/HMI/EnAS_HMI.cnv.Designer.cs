﻿/*
 * Created by nxtSTUDIO.
 * User: Aalto_IT
 * Date: 9/30/2016
 * Time: 2:36 AM
 * 
 */
using System;
using System.ComponentModel;
using System.Collections;
using System.Diagnostics;

using NxtControl.GuiFramework;

namespace HMI.Main.Canvases
{
	/// <summary>
	/// Summary description for EnAS_HMI.
	/// </summary>
	partial class EnAS_HMI
	{
		#region Component Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.orderHMI = new HMI.Main.Symbols.ProductOrder.sDefault();
			// 
			// orderHMI
			// 
			this.orderHMI.BeginInit();
			this.orderHMI.AngleIgnore = false;
			this.orderHMI.DesignTransformation = new NxtControl.Drawing.Matrix(1D, 0D, 0D, 1D, 4D, 13D);
			this.orderHMI.Name = "orderHMI";
			this.orderHMI.SecurityToken = ((uint)(4294967295u));
			this.orderHMI.TagName = "C3D6AB897888CDC0";
			this.orderHMI.EndInit();
			// 
			// EnAS_HMI
			// 
			this.Bounds = new NxtControl.Drawing.RectF(((float)(0D)), ((float)(0D)), ((float)(1112D)), ((float)(844D)));
			this.Brush = new NxtControl.Drawing.Brush("White");
			this.Name = "EnAS_HMI";
			this.Shapes.AddRange(new System.ComponentModel.IComponent[] {
									this.orderHMI});
			this.Size = new System.Drawing.Size(1112, 844);
		}
		private HMI.Main.Symbols.ProductOrder.sDefault orderHMI;
		#endregion
	}
}
