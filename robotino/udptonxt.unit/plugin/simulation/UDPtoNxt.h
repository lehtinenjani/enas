#ifndef _UDPtoNxt_H_
#define _UDPtoNxt_H_

#include "plugin/simulation/Unit.h"

class UDPtoNxt : public plugin::simulation::Unit
{
public:
	UDPtoNxt (plugin::simulation::UnitDelegate& del);
	~UDPtoNxt();

	void step (void);

private:
	int xout;
	int yout;
	int poseout;
	int dockout;
	char dockout_str[4];
	int out0;
	int out1;
	int out2;
	int out3;
	int out4;
	int out5;
	int out6;
};

#endif // _UDPtoNxt_H_
