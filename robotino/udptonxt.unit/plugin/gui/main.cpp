#include "plugin/gui/Interface.h"
#include "UDPtoNxt.h"

BEGIN_GUI_INTERFACE( "blockdesigner", "UDPtoNxt" )
	BEGIN_UNITWIDGETS
		ADD_UNITWIDGET( "blockdesigner UDPtoNxt", "dialog", UDPtoNxt )
	END_UNITWIDGETS
END_GUI_INTERFACE
